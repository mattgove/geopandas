#!/usr/bin/env python3
import geopandas
import matplotlib.pyplot as plt
import contextily as ctx

# Read in Shapefile
shp_path = "shp/mexico/mexstates.shp"
mexico = geopandas.read_file(shp_path)

# Convert to Web Mercator Projection
mexico = mexico.to_crs(epsg=3857)

# Plot the Map Tiles
ax = mexico.plot(figsize=(12, 8), alpha=0.5, edgecolor="k")

# Add Basemap
ctx.add_basemap(ax) #, zoom=12)

# Save Figure to Hard Drive
plt.savefig("mexico-state-borders.png")