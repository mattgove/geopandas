#!/usr/bin/env python3
""" 
Script to be used with our tutorial "How to Create a Choropleth Map
Using Region Mapping", which was published on 23 July, 2021.

Usage: Open a Terminal or Command Prompt window and execute the
following command:
    python3 choropleth-region-mapping.py
"""
import geopandas
import pandas
import matplotlib.pyplot as plt
import contextily as ctx

GEOJSON_FILE = "geojson/USStates.geojson"
CSV_FILE = "csv/usa-covid-20210102.csv"

# 3857 - Mercator Projection
XBOUNDS = (-1.42e7, -0.72e7)
YBOUNDS = (0.26e7, 0.66e7)



def choropleth_map(mapped_dataset, column, plot_type):
    """ Generates a choropleth map from a Pandas DataFrame object and saves it to a *.png file
    
    Args:
        mapped_dataset (pandas.DataFrame): The dataset that has merged/linked the CSV data to the GeoJSON
        column (str): The name of the column to plot on the choropleth map
        plot_type (str): The plot type (e.g. "New Daily Cases") that goes in the plot's title. 
    
    Returns:
        [Void]
    """
    # Initial Figure
    ax = mapped_dataset.plot(figsize=(12,6), column=column, alpha=0.75, legend=True, cmap="YlGnBu", edgecolor="k")
    
    # Remove axis ticks because we are not using the WGS-84/EPSG:4326 projection, so
    # they will not be in degrees of latitide and longitude
    ax.set_xticks([])
    ax.set_yticks([])

    # Set Labels
    title = "COVID-19: {} in the United States\n2 January, 2021".format(plot_type)
    ax.set_title(title)

    # Bound to Lower 48
    ax.set_xlim(*XBOUNDS)
    ax.set_ylim(*YBOUNDS)

    # Add Basemap
    ctx.add_basemap(ax, crs=full_dataset.crs.to_string(), source=ctx.providers.Stamen.TonerLite, zoom=4)

    # Output Map to *.png File
    output_path = "covid19_{}_usa.png".format(column)
    plt.savefig(output_path)
    
    return None



# Read in US State Boundary Geometry from GeoJSON
raw_geojson = geopandas.read_file(GEOJSON_FILE)

# Set Initial Projetion to WGS-84/EPSG:4326, as defined in the GeoJSON
geojson_4326 = raw_geojson.set_crs(4326)

# Convert to Mercator Projection (EPSG:3857) for better map aspect ratio
geojson = geojson_4326.to_crs(epsg=3857)

# Read in Data from CSV
data = pandas.read_csv(CSV_FILE)

# Merge the Data Set with the Geometry
full_dataset = geojson.merge(data,  left_on="STATE_ID", right_on="iso3166_2") # right_on="id")

# Generate Plot
columns_to_plot = [
    "new_cases",
    "confirmed",
    "new_deaths",
    "dead"
]

# Plot Types for the Title
plot_types = [
    "New Daily Cases",
    "Total Cumulative Cases",
    "New Daily Deaths",
    "Total Cumulative Deaths"
]

# Generate 3 Choropleth Maps
for column, plot_type in zip(columns_to_plot, plot_types):
    choropleth_map(full_dataset, column, plot_type)
    print("Successfully Generated Choropleth Map for {}...".format(plot_type))