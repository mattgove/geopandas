#!/usr/bin/env python3
""" This script demonstrates how easy it is to spread disinformation
about the COVID-19 pandemic through maps.
"""
import pandas
import geopandas
import numpy as np
import contextily as ctx
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import re

# Data Files
COVID_DATAFILE = "data/covid-by-county.csv"
COUNTIES_GEOJSON = "geometry/USCounties.geojson"

# 3857 - Mercator Projection
XBOUNDS = (-1.42e7, -0.72e7)
YBOUNDS = (0.26e7, 0.66e7)



# Read in COVID-19 Data and the County GeoJSON
covid_data = pandas.read_csv(COVID_DATAFILE)
geometry = geopandas.read_file(COUNTIES_GEOJSON)

# Link the COVID-19 Data to the County Outlines Based on County ID
dataset = geometry.merge(covid_data, left_on="CountyID", right_on="county_id")

# Convert the Merged Dataset to the Mercator Projection (EPSG:3857)
dataset = dataset.to_crs(3857)



# EXAMPLE 1: Generate misleading COVID-19 maps by plotting the exact same
# dataset, but changing the upper limits of the color bar from 5 new daily 
# cases to 15,000 new daily cases
color_bar_max_limits = [5 , 15000]

for color_bar_max_limit in color_bar_max_limits:
    # Set the Color Bar Range
    norm = colors.Normalize(vmin=0, vmax=color_bar_max_limit)

    # Initialize Plot
    ax = dataset.plot(figsize=(20,10), column="new_cases", alpha=0.75, legend=True)
    ax.set_xticks([])
    ax.set_yticks([])

    # Set the Plot's Title
    title = "New Daily COVID-19 Cases: 18 July, 2021"
    ax.set_title(title)

    # Bound to Lower 48
    ax.set_xlim(*XBOUNDS)
    ax.set_ylim(*YBOUNDS)

    # Add a Basemap
    ctx.add_basemap(ax, crs=dataset.crs.to_string(), source=ctx.providers.Stamen.TonerLite, zoom=5)

    # Output the Map to a .png File
    output_filename = title.lower().replace(" ", "-")
    output_file = "{}-{}.png".format(output_filename, color_bar_max_limit)
    plt.savefig(output_file, bbox_inches="tight")



# EXAMPLE 2: Generate misleading COVID-19 maps by maipulating the break
# points in the color bar
norm = colors.Normalize(vmin=0, vmax=1500)  # Color Bar Range

# Set the Number of Colors to Put in the Color Bar
num_colors = 5

# The matplotlib Color Map to Use
cmap = "YlGnBu"

# Set the Color Bar Break Points
bins = [1300, 1350, 1400, 1450]
kwds = {"bins": bins}

# Generate a New Color Bar Based on the New Color Bar Break Points
cm = plt.get_cmap(cmap)
scheme = cm(1. * np.arange(num_colors)/num_colors)
cmap_obj = colors.ListedColormap(scheme)

# Initialize Map
ax = dataset.plot(figsize=(20,10), column="new_cases", alpha=0.75, legend=True, cmap=cmap_obj, scheme="User_Defined", classification_kwds=kwds, edgecolor="k")
ax.set_xticks([])
ax.set_yticks([])

# Set the Map's Title
title = "New Daily COVID-19 Cases: 18 July, 2021"
ax.set_title(title)

# Bound to Lower 48
ax.set_xlim(*XBOUNDS)
ax.set_ylim(*YBOUNDS)

# Add a Basemap
ctx.add_basemap(ax, crs=dataset.crs.to_string(), source=ctx.providers.Stamen.TonerLite, zoom=5)

# Output Map to a .png File
output_filename = title.lower().replace(" ", "-")
output_file = "{}-altered-colorbreaks.png".format(output_filename)
plt.savefig(output_file, bbox_inches="tight")



# EXAMPLE 3: Generate misleading COVID-19 maps by plotting the exact same
# dataset, but reducing the number of colors in the color bar from 5 to 2
norm = colors.Normalize(vmin=0, vmax=1500)  # Color Bar Range

# Set the Number of Colors to Put in the Color Bar
num_colors = 2

# The matplotlib Color Map to Use
cmap = "YlGnBu"

# Generate a New Color Bar with the Number of Colors Indicated in num_colors
cm = plt.get_cmap(cmap)
scheme = cm(1. * np.arange(num_colors)/num_colors)
cmap_obj = colors.ListedColormap(scheme)

# Initialize Map
ax = dataset.plot(figsize=(20,10), column="new_cases", alpha=0.75, legend=True, cmap=cmap_obj, edgecolor="k", norm=norm)
ax.set_xticks([])
ax.set_yticks([])

# Set the Map's Title
title = "New Daily COVID-19 Cases: 18 July, 2021"
ax.set_title(title)

# Bound to Lower 48
ax.set_xlim(*XBOUNDS)
ax.set_ylim(*YBOUNDS)

# Add a Basemap
ctx.add_basemap(ax, crs=dataset.crs.to_string(), source=ctx.providers.Stamen.TonerLite, zoom=5)

# Output Map to a .png File
output_filename = title.lower().replace(" ", "-")
output_filename = re.sub(r"[^A-Za-z0-9-]+", "", output_filename)
output_file = "{}-fewer-colorbreaks.png".format(output_filename)
plt.savefig(output_file, bbox_inches="tight")