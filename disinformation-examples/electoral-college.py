#!/usr/bin/env python3
import pandas
import geopandas
import contextily as ctx
import matplotlib.pyplot as plt

EC_RESULTS_FILE = "data/electoral-college-results.csv"
GEOMETRY_FILE = "geometry/USStates.geojson"
EPSG = 3857

# 3857 - Mercator Projection
XBOUNDS = (-1.42e7, -0.72e7)
YBOUNDS = (0.26e7, 0.66e7)




# Read in Data and Geometry
ec_data = pandas.read_csv(EC_RESULTS_FILE)
ec_data["ID"] = ec_data["ID"].astype(str)

# Convert Geometry to Mercator Projection (EPSG:3857)
geometry = geopandas.read_file(GEOMETRY_FILE)
geometry = geometry.to_crs(EPSG)

geometry["geometry"] = geometry["geometry"].to_crs(EPSG).centroid.to_crs(EPSG)

# Merge the CSV File with the GeoJSON
dataset = geometry.merge(ec_data, left_on="STATE_ID", right_on="ID")

# Years to Plot
years = [2020, 2016]

# Generate Maps
for year in years:
    column = "{}_Winner".format(year)
    markersize = dataset["Votes"] * 30

    ax = dataset.plot(figsize=(20,10), column=column, alpha=0.75, legend=True, cmap="bwr_r", markersize=markersize)
    ax.set_xticks([])
    ax.set_yticks([])

    title = "{} Electoral College Results".format(year)
    ax.set_title(title)

    # Bound to Lower 48
    ax.set_xlim(*XBOUNDS)
    ax.set_ylim(*YBOUNDS)

    # Add Basemap
    ctx.add_basemap(ax, crs=dataset.crs.to_string(), source=ctx.providers.Stamen.TonerLite, zoom=5)

    # Output File
    output_filename = title.lower().replace(" ", "-")
    output_file = "{}.png".format(output_filename)
    plt.savefig(output_file, bbox_inches="tight")