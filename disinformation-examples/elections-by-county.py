#!/usr/bin/env python3
import pandas
import geopandas
import contextily as ctx
import matplotlib.pyplot as plt
import matplotlib.colors as colors

ELECTION_DATA_CSV = "data/countypres_2000-2020.csv"
COUNTY_GEOJSON = "geometry/USCounties.geojson"
PARTIES = ["DEMOCRAT", "REPUBLICAN"]

# 3857 - Mercator Projection
XBOUNDS = (-1.42e7, -0.72e7)
YBOUNDS = (0.26e7, 0.66e7)




def election_year_dataframe(year, raw_data):
    """ Generates a Pandas DataFrame object of all election data for a given year

    :param year: The Election Year
    :type year: int
    :param raw_data: The full dataset that was loaded from the CSV File (ELECTION_DATA_CSV)
    :type raw_data: pandas.DataFrame
    :return: A filtered dataset containing election data for the given year
    :rtype: pandas.DataFrame
    """
    election_data = raw_data[(raw_data["year"] == year)]

    # We will parse the data into a Python dictionary, then convert said
    # dictionary to a Pandas DataFrame Object
    data_dict = dict()

    for row in election_data.iterrows():
        # Extract Row Data from DataFrame.iterrows() Tuple
        row = row[1]
        
        try:
            fips = str(int(row["county_fips"]))
            while len(fips) < 5:
                fips = "0{}".format(fips)
        except:
            continue

        # Parse the Data in the Row
        county = row["county_name"]
        state = row["state"]
        candidate = row["candidate"]
        party = row["party"]

        # Only Include Democrat or Republican Votes
        if party not in PARTIES:
            continue

        # In some states like Virginia, you have city counties and regular counties with the same name.
        # Votes are filed under the regular county name, with "NA" on the city name. This skips
        # The NA's.
        try:
            candidate_votes = int(row["candidatevotes"])
            total_votes = int(row["totalvotes"])
        except:
            continue

        candidate_key = "{}_candidate".format(party.lower())
        candidate_votes_key = "{}_votes".format(party.lower())


        # If FIPS Code not in dictionary, add it
        if fips not in data_dict.keys():
            data_dict[fips] = dict()
        
        # Populate Dictionary
        row_keys = data_dict[fips].keys()
        
        if "county" not in row_keys:
            data_dict[fips]["county"] = county
        
        if "state" not in row_keys:
            data_dict[fips]["state"] = state

        if "total_votes" not in row_keys:
            data_dict[fips]["total_votes"] = total_votes

        if candidate_votes_key not in row_keys:
            data_dict[fips][candidate_votes_key] = 0

        data_dict[fips][candidate_key] = candidate
        data_dict[fips][candidate_votes_key] += candidate_votes

    for fips, data in data_dict.items():
        # Democrat Candidate Data
        dem_candidate = data["democrat_candidate"]
        dem_votes = data["democrat_votes"]

        # Republican Candidate Data
        rep_candidate = data["republican_candidate"]
        rep_votes = data["republican_votes"]

        # Total Number of Votes Cast (including third part votes)
        total_votes = data["total_votes"]

        # Percentage of Total Votes Each Candidate Received
        dem_pct = round(100 * dem_votes / total_votes, 1)
        rep_pct = round(100 * rep_votes / total_votes, 1)

        # Determine Winner
        if dem_votes > rep_votes:
            winner = dem_candidate
            winner_pct = dem_pct
            winning_votes = dem_votes
        elif rep_votes > dem_votes:
            winner = rep_candidate
            winner_pct = rep_pct
            winning_votes = rep_votes
        else:
            winner = "TIE"
            winner_pct = None
            winning_votes = None
        
        # Populate the Rest of the Dictionary
        data_dict[fips]["democrat_pct"] = dem_pct
        data_dict[fips]["republican_pct"] = rep_pct
        data_dict[fips]["winner"] = winner
        data_dict[fips]["margin"] = abs(dem_votes - rep_votes)
        data_dict[fips]["winner_pct"] = winner_pct
        data_dict[fips]["winning_votes"] = winning_votes

    # Create a Pandas DataFrame object from the dictionary
    df = pandas.DataFrame.from_dict(data_dict, orient="index")
    return df


def choropleth_map(all_data, column, title, cmap="YlGnBu", norm=None):
    """ Generates a Choropleth Map From a GeoPandas DataDrame Object

    :param all_data: The dataset you wish to plot. It should be passed
        either as a merged pandas.DataFrame object or as a tuple in the
        format (data, geometry). If you pass a tuple, this function will
        attempt to merge the datasets into a single DataFrame object.
    :type all_data: pandas.DataFrame
    :param column: The column or parameter you wish to plot
    :type column: str
    :param title: The title that goes at the top of the map
    :type title: str
    :param cmap: a MatPlotLib color map (cmap), defaults to "YlGnBu"
    :type cmap: str, optional
    :param norm: Normalization of the color bar, defaults to None
    :type norm: matplotlib.colors.Normalize, optional
    """
    crs = 3857
    crs_string = "EPSG:{}".format(crs)
    if type(all_data) == tuple:
        # Merge Data
        data = all_data[0]
        geometry = all_data[1]
        counties_geometry = geometry.to_crs(crs)

        # Merge the data
        dataset = counties_geometry.merge(data, left_on="FIPS", right_index=True)
    else:
        # Do Not Merge Data
        all_data.set_crs(epsg=4326)
        dataset = all_data.to_crs(epsg=crs)

    ax = dataset.plot(figsize=(20,10), column=column, alpha=0.75, legend=True, cmap=cmap, norm=norm, edgecolor="k") #, markersize=markersize)
    ax.set_xticks([])
    ax.set_yticks([])

    ax.set_title(title)

    # Bound to Lower 48
    ax.set_xlim(*XBOUNDS)
    ax.set_ylim(*YBOUNDS)

    ctx.add_basemap(ax, crs=crs_string, source=ctx.providers.Stamen.TonerLite, zoom=5)

    output_filename = title.lower().replace(" ", "-")
    output_file = "{}.png".format(output_filename)
    plt.savefig(output_file, bbox_inches="tight")
    return None


def voter_turnout_difference(year_recent, year_old, data):
    """Generates a map of difference in the number of votes cast for the winning
    candidate in one election vs a previous election. It is designed to compare
    an election to the most recent one before it (i.e. 2020 vs 2016), but you can
    compare any two elections you wish.

    :param year_recent: The more recent of the two years you wish to compare
    :type year_recent: int
    :param year_old: The older of the two years you wish to compare
    :type year_old: int
    :param data: The full election dataset
    :type data: pandas.DataFrame
    :return: Void
    :rtype: None
    """
    # How Many More People Voted in Recent Year vs Old Year
    df_recent = election_year_dataframe(year_recent, data)
    df_old = election_year_dataframe(year_old, data)

    # We will store the data in a dictionary, and convert it to a DataFrame at the end.
    diff_dict = dict()

    for county_old_tuple in df_old.iterrows():
        fips = county_old_tuple[0]
        county_old = county_old_tuple[1]

        # Skip Counties or Regions that Do Not Have a FIPS Code
        try:
            county_recent = df_recent.loc[fips]
        except:
            continue

        # We want 3 differences: Total, Democrat, and Republican
        total_diff = county_recent["total_votes"] - county_old["total_votes"]
        dem_diff = county_recent["democrat_votes"] - county_old["democrat_votes"]
        rep_diff = county_recent["republican_votes"] - county_old["republican_votes"]

        if dem_diff > rep_diff:
            winner = "Democrat"
        elif rep_diff > dem_diff:
            winner = "Republican"
        else:
            continue
            
        # Put the Data into a Dictionary
        diff_dict[fips] = {
            "total": total_diff,
            "democrat": dem_diff,
            "republican": rep_diff,
            "scaled_diff": dem_diff - rep_diff,
            "vote_diff_winner": winner,
        }

    # Convert the Dictionary into a Pandas DataFrame Object
    voter_turnout = pandas.DataFrame.from_dict(diff_dict, orient="index")
    vt_norm = colors.Normalize(vmin=-50000, vmax=50000)

    # Party Gains
    title = "Voter Turnout Difference: {} vs {}".format(year_recent, year_old)
    data = (voter_turnout, counties_geometry)
    choropleth_map(data, "scaled_diff", title, cmap="bwr_r", norm=vt_norm)

    # Total Voter Turnout Map
    title = "Total Voter Turnout Changes: {} vs {}".format(year_recent, year_old)
    choropleth_map(data, "total", title, cmap="PiYG", norm=vt_norm)
    return None


def election_data_by_population(year, raw_data, geometry):
    """ Generates a Map of Election Results using scaled dots to mark each county
    instead of a choropleth map. The dots are colored to indicate which candidate
    won and scaled (size) based on the number of votes cast for the winning candidate.

    :param year: The Election Year
    :type year: int
    :param raw_data: The raw (unmerged) election dataset, loaded from the CSV file
    :type raw_data: pandas.DataFrame
    :param geometry: The county borders, loaded from the GeoJSON
    :type geometry: pandas.DataFrame
    :return: Void
    :rtype: None
    """
    # Make a Copy of the Geometry to Convert Polygons to Centroid Points
    county_points = geometry.copy()

    # Use the Mercator Projection (EPSG:3857)
    epsg = 3857
    
    # Convert Polygon Borders to a Single Point Located at the Center of Each County
    county_points['geometry'] = county_points['geometry'].to_crs(epsg).centroid.to_crs(epsg)
    
    # Generate a Pandas DataFrame for the Election Year
    df_2020 = election_year_dataframe(year, raw_data)

    # Link the Election Data to the Points Located at the Center of Each County
    dataset = county_points.merge(df_2020, left_on="FIPS", right_index=True)

    # Scale the Dots Marking Each County Based on the Numer of Votes the Winning Candidate Received
    markersize = dataset["winning_votes"] / 1e4

    # Initialize Plot
    ax = dataset.plot(figsize=(20,10), column="winner", alpha=0.75, legend=True, cmap="bwr_r", markersize=markersize)
    ax.set_xticks([])
    ax.set_yticks([])

    title = "{} Election Results Normalized by Number of Votes Cast for Winning Candidate".format(year)
    ax.set_title(title)

    # Bound to Lower 48
    ax.set_xlim(*XBOUNDS)
    ax.set_ylim(*YBOUNDS)

    # Add Basemap
    ctx.add_basemap(ax, crs="EPSG:3857", source=ctx.providers.Stamen.TonerLite, zoom=5)

    # Output File
    output_filename = title.lower().replace(" ", "-")
    output_file = "{}.png".format(output_filename)
    plt.savefig(output_file, bbox_inches="tight")
    return None




# Generate 2 Map of Election Results By County: A Choropleth Map
# and a Map of the Scaled Dots or Points Marking Each County
presidential_election_data = pandas.read_csv(ELECTION_DATA_CSV)
counties_geometry = geopandas.read_file(COUNTY_GEOJSON)
years = [2004, 2016, 2020]

for year in years:
    # Generate a Pandas DataFrame for the given year's election data
    df = election_year_dataframe(year, presidential_election_data)
    title = "{} Election Results By County".format(year)
    data = (df, counties_geometry)

    # Generate Choropleth Map
    choropleth_map(data, "winner", title, cmap="bwr_r")
    print("Successfully generated {}...".format(title))

    # Generate Normalized Map
    election_data_by_population(year, presidential_election_data, counties_geometry)
    print("Successfully generated normalized election map for {}...".format(year))


# Population Choropleth Map
pops_norm = colors.Normalize(vmin=0, vmax=1e6)
choropleth_map(counties_geometry, "Population", "U.S. Population by County", cmap="YlOrRd", norm=pops_norm)


# Voter Turnout Difference Maps
voter_turnout_difference(2020, 2016, presidential_election_data)    # 2020 vs 2016
voter_turnout_difference(2016, 2012, presidential_election_data)    # 2016 vs 2012