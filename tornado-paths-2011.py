#!/usr/bin/env python3
import geopandas
import pandas as pd
import matplotlib.pyplot as plt
import contextily as ctx


def plot_top10(sorted_list, title):
    """ Generates a bar chart for the top 10 most/fewest tornadoes by state

    Args:
        sorted_list (list): Sorted list of data, after being run through the sorted() function
        title (str): The title of the plot. The title will be prominently displayed on the top of the plot
    
    Returns:
        This function does not return any values. Instead, it outputs the figure in .png format to the
        same directory that this script is located in.
    """
    states = []
    num_tornadoes = []
    
    # Count how many tornadoes occurred in each state
    for pair in sorted_list:
        state = pair[0]
        num = pair[1]

        states.append(state)
        num_tornadoes.append(num)

    # Define X and Y Data to Plot
    data_frame = {
        "Number of Tornadoes": num_tornadoes,
        "States": states
    }

    # Plot the Data
    df = pd.DataFrame(data_frame, index=states)
    ax = df.plot.bar(rot=0, title=title)
    ax.set_xlabel("State")
    ax.set_ylabel("Number of Tornadoes")

    # Save the Figure
    figname = "{}.png".format(title)
    plt.savefig(figname)


def plot_map(data, region, xlim=None, ylim=None):
    """ Generates a geographic map of tornado paths

    Args:
        data (dict): The data (filtered or raw) extracted from the shapefile
        region (str): The region being displayed on the map. This value is used in the plot title and filename.
        xlim (tuple): The x (longitude) limits of the geographic map, in the form (min, max)
        ylim (tuple): The y (latitude) limits of the geographic map, in the form (min, max)
    
    Returns:
        This function does not return any values. Instead, it outputs the figure in .png format to the
        same directory that this script is located in.
    """
    ax = data.plot(figsize=(12,6), column="mag", legend=True, cmap="cool")
    
    # Set Bounding Box
    if xlim:
        ax.set_xlim(*xlim)
    if ylim:
        ax.set_ylim(*ylim)

    # Set Title
    title = "2011 Tornado Map: {}".format(region)
    fname_region = region.replace(" ", "-").lower()
    ax.set_title(title)
    ax.set_xlabel("Longitude")
    ax.set_ylabel("Latitude")
    
    # Add Basemap
    ctx.add_basemap(ax, crs=data.crs.to_string(), source=ctx.providers.Stamen.TonerLite)

    # Save Figure
    figname = "2011-tornadoes-{}.png".format(fname_region)
    plt.savefig(figname)


# Read in the Shapefile
shp_path = "shp/1950-2018-torn-aspath/1950-2018-torn-aspath.shp"
all_tornadoes = geopandas.read_file(shp_path)

# Filter the Data to Only Use 2011 Tornadoes
filtered_data = all_tornadoes[(all_tornadoes["yr"] == 2011) & (all_tornadoes["st"] != "HI") & (all_tornadoes["st"] != "PR")]
filtered_data = filtered_data.to_crs(epsg=4236)


# Plot the United States
plot_map(filtered_data, "United States", (-110, -70))

# Plot Tornado Alley
plot_map(filtered_data, "Tornado Alley", (-100, -91), (33, 38))

# Plot Dixie Alley
plot_map(filtered_data, "Dixie Alley Super Outbreak", (-95, -81), (29, 37))


# Generate Bar Charts
states = filtered_data["st"]
state_counts = dict()
for state in states:
    # Filter Out DC and Puerto Rico
    if state in ["DC", "PR"]:
        continue

    if state in state_counts.keys():
        state_counts[state] += 1
    else:
        state_counts[state] = 1

fewest_counts = sorted(state_counts.items(), key=lambda x: x[1])[:10]
most_counts = sorted(state_counts.items(), key=lambda x: x[1], reverse=True)[:10]
plot_top10(most_counts, "US States with Most Tornadoes in 2011")
plot_top10(fewest_counts, "US States with Fewest Tornadoes in 2011")